package com.example.calculadora

class Operaciones (private val num1: Float, private val num2: Float) {

    public fun sumar():Float{
        return this.num1 + this.num2
    }

    public fun restar():Float{
        return this.num1 - this.num2
    }

    public fun multiplicar():Float{
        return this.num1 * this.num2
    }

    public fun dividir():Float{
        return this.num1 / this.num2
    }

}