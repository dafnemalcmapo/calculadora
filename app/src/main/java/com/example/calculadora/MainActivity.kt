package com.example.calculadora

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat

class MainActivity : AppCompatActivity() {
    private lateinit var txtUsuario: EditText
    private lateinit var txtContraseña: EditText
    private lateinit var btnIngresar: Button
    private lateinit var btnCerrar: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContentView(R.layout.activity_main)
        iniciarComponentes()
        eventoClic()
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }
    }

    private fun iniciarComponentes() {
        txtUsuario = findViewById(R.id.txtUsuario)
        txtContraseña = findViewById(R.id.txtContraseña)
        btnCerrar = findViewById(R.id.btnCerrar)
        btnIngresar = findViewById(R.id.btnIngresar)
    }

    private fun eventoClic() {
        btnIngresar.setOnClickListener {
            val usuario = getString(R.string.usuario)
            val contraseña = getString(R.string.contraseña)
            val nombre = getString(R.string.nombre)

            if (txtUsuario.text.toString() == usuario && txtContraseña.text.toString() == contraseña) {
                val intent = Intent(this, OperacionesActivity::class.java)
                intent.putExtra("usuario", nombre)
                startActivity(intent)
            } else {
                Toast.makeText(this, "Falta información o no corresponde al usuario", Toast.LENGTH_SHORT).show()
            }
        }

        btnCerrar.setOnClickListener {
            finish()
        }
    }
}
